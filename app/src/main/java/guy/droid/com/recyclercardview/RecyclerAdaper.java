package guy.droid.com.recyclercardview;

import android.animation.AnimatorInflater;
import android.animation.StateListAnimator;
import android.annotation.TargetApi;
import android.graphics.Color;
import android.os.Build;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.balysv.materialripple.MaterialRippleLayout;

import java.util.ArrayList;

import tyrantgit.explosionfield.ExplosionField;

import static android.os.Build.*;

/**
 * Created by admin on 6/7/2016.
 */
public class RecyclerAdaper extends RecyclerView.Adapter<RecyclerAdaper.ViewHolder> {
    MainActivity activity;
    ArrayList<String> names;
    ArrayList<String> desg;
    ExplosionField mExplosionField;
    Animation animation;


    public RecyclerAdaper(MainActivity activity,ArrayList<String> names, ArrayList<String> dest) {
        this.names = names;
        this.desg = dest;
        this.activity = activity;
        mExplosionField = ExplosionField.attach2Window(activity);
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

       // return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_child,parent,false));
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        animation = AnimationUtils.loadAnimation(activity, R.anim.push_down_in);
        return new ViewHolder(
                MaterialRippleLayout.on(inflater.inflate(R.layout.recycler_child, parent, false))
                        .rippleOverlay(true)
                        .rippleAlpha(0.2f)
                        .rippleColor(Color.BLACK).rippleFadeDuration(1000)
                        .rippleHover(true).rippleDuration(1000)
                        .create()
        );
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.textView.setText(names.get(position));
        holder.textView2.setText(desg.get(position));
       // holder.cardView.setAnimation(holder.animation);
        if(VERSION.SDK_INT >= VERSION_CODES.LOLLIPOP)
        {
            holder.cardView.setStateListAnimator(holder.animator);

        }
        holder.cardView.setAnimation(animation);
        holder.cardView.setOnClickListener(Colored(position,holder));
        holder.delete.setOnClickListener(Delete(position,holder));

    }

    @Override
    public int getItemCount() {
        return names.size();
    }
    public View.OnClickListener Delete(final int position, final ViewHolder holder)
    {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // showEditDialog(position, holder);
                mExplosionField.explode(holder.cardView);
                names.remove(position);
                desg.remove(position);
                //   context.updateAdapter();

                activity.updater();
                //  holder.cardView.setBackgroundColor(Color.GREEN);

            }
        };
    }
    public View.OnClickListener Colored(final int position, final ViewHolder holder)
    {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // showEditDialog(position, holder);
                mExplosionField.explode(v);
            //    names.remove(position);
             //   desg.remove(position);
             //   context.updateAdapter();
                holder.cardView.setBackgroundColor(Color.GREEN);
                activity.updater();


            }
        };
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView textView,textView2;
        CardView cardView;
        ImageView delete;
        int v = VERSION.SDK_INT;

      StateListAnimator animator;
        public ViewHolder(View itemView) {
            super(itemView);


            textView = (TextView)itemView.findViewById(R.id.texts);
            textView2 = (TextView)itemView.findViewById(R.id.dest);
            cardView = (CardView)itemView.findViewById(R.id.cards);
            delete = (ImageView)itemView.findViewById(R.id.delete);

            if(VERSION.SDK_INT >= VERSION_CODES.LOLLIPOP)
            {


            }
            textView2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    System.out.println(names.get(getAdapterPosition()));
                }
            });

        }
    }
}
