package guy.droid.com.recyclercardview;

import android.content.Intent;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.OvershootInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import jp.wasabeef.recyclerview.animators.SlideInLeftAnimator;
import jp.wasabeef.recyclerview.animators.SlideInUpAnimator;

public class MainActivity extends AppCompatActivity {
RecyclerView recyclerView;
ArrayList<String> names;
ArrayList<String> dest;
    Animation animation;
    EditText named,destination;
    Button button;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        named = (EditText)findViewById(R.id.name);
        destination = (EditText)findViewById(R.id.fields);
        button = (Button) findViewById(R.id.add_button);


        HashMap<String,String> hashMap = new HashMap<>();
        hashMap.put("name","raz");
        hashMap.put("nameS","DA");
        hashMap.put("nameSA","AS");
        hashMap.put("nameAS","AA");
        JSONObject jsonObject = new JSONObject(hashMap);
        JSONArray jsonArray = new JSONArray();
        jsonArray.put(jsonObject);
        jsonArray.put(jsonObject);
        jsonArray.put(jsonObject);
        Log.d("JSON IS",jsonObject+"");
        Log.d("JSON ARRIS",jsonArray+"");
        recyclerView = (RecyclerView)findViewById(R.id.recycler);
        recyclerView.setHasFixedSize(true);
        animation = AnimationUtils.loadAnimation(this, R.anim.slide_in_right);
        names = new ArrayList<>();
        dest = new ArrayList<>();


/*        names.add("Mohamed Rashik");
        names.add("Adarsh");
        names.add("Darsak");
        names.add("ID");
        dest.add("Hybrid");
        dest.add("iOS");
        dest.add("Android"); */

        GridLayoutManager gridLayoutManager = new GridLayoutManager(this,3);
       //recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setLayoutManager(gridLayoutManager);
      //  recyclerView.setItemAnimator(new SlideInUpAnimator(new OvershootInterpolator(1f)));
      //  recyclerView.setAdapter(new RecyclerAdaper(names,dest));
        recyclerView.setAnimation(animation);

        String secure = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                names.add(named.getText().toString());
                dest.add(destination.getText().toString());
                recyclerView.swapAdapter(new RecyclerAdaper(MainActivity.this,names,dest),true);
            }
        });
      //  startActivityForResult(new Intent(Settings.ACTION_SHOW_REGULATORY_INFO), 0);
    }
    public void updater()
    {
        recyclerView.swapAdapter(new RecyclerAdaper(MainActivity.this,names,dest),true);
    }
/** Libraries added for material ripple https://github.com/balysv/material-ripple
 https://github.com/tyrantgit/ExplosionField
 https://github.com/wasabeef/recyclerview-animators

 * **/
}
